const express = require('express');
const natural = require('natural');
const router = express.Router();
var Sentiment = require('sentiment');
var Twitter = require('twitter');
var configTwitter = require('../tools/TwitterConfig.js');
const aposToLexForm = require('apos-to-lex-form');
const SpellCorrector = require('spelling-corrector');

const spellCorrector = new SpellCorrector();
spellCorrector.loadDictionary();
const SW = require('stopword');

var T = new Twitter(configTwitter);


router.get('/getTweets/:word/:lang', (req,res,next) => {

  wordToAnalyze = req.params.word;
  lang = req.params.lang;

  console.log(wordToAnalyze);
  console.log(lang);

    var params = {
        q: wordToAnalyze,
        count: 1000,
        result_type: 'recent',
        lang: lang
      }

      T.get('search/tweets', params, function(err, data, response) {
        if(!err){
            let text = [];
            let nbPositifsTweets = 0;
            let nbNegatifTweets = 0;
            let nbNeutralTxeets = 0;


            for(let i= 0; i < data.statuses.length; i++) {

                // Récupérer chaque tweet 
                const contentTweet = data.statuses[i].text; 
                // Gestion des contractions comme I'm You're ==> I am, You are
                const lexForm = aposToLexForm(contentTweet)
                // En miniscule
                const lowerCase = lexForm.toLowerCase();
                // Suppression des caractères spéciaux 
                const alphaOnlyReview = lowerCase.replace(/[^a-zA-Z\s]+/g, '');

                // Séparation de chaque mot d'une phrase à l'aide de 
                const { WordTokenizer } = natural;
                const tokenizer = new WordTokenizer();
                const tokenizedReview = tokenizer.tokenize(alphaOnlyReview);

                // Verification et correction de chaque mot
                // tokenizedReview.forEach((word,index) => {
                //   tokenizedReview[index] = spellCorrector.correct(word);
                // })

                // Suppresion des 'Stop words' qui ne changent rien au sentiment d'une phrase comme but, a ,or, what
                const filteredReview = SW.removeStopwords(tokenizedReview);

                const { SentimentAnalyzer, PorterStemmer } = natural;
                const analyzer = new SentimentAnalyzer('English', PorterStemmer, 'afinn');
                const analysis = analyzer.getSentiment(filteredReview);

                if(analysis > 0) { 
                  nbPositifsTweets++;
                } else if (analysis < 0) {
                  nbNegatifTweets++; 
                } else {
                  nbNeutralTxeets++;
                }
                // console.log(analysis)
                console.log("*** "+lowerCase)

                text.push({
                    "data" : contentTweet,
                    "resultat" : analysis
                  });
            }
            res.send({
              "text" : text,
              "nbPositif" : nbPositifsTweets,
              "nbNegatif" : nbNegatifTweets,
              "nbNeutre" : nbNeutralTxeets 
            });
               
        } else {
          console.log(err);
        }
      })
});

module.exports = router;