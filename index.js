require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const colors = require("colors");
const cors = require('cors');
const tweets = require('./routes/tweets');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(morgan('dev'));

app.use('/api', tweets);

app.use((err, req, res, next) => {
    res.status(422).send({ error : err.message});
    console.log(err);
});

app.get('/', function (req, res) {
    res.send('Hello World!')
  })
  
app.listen(process.env.PORT || 5000, () => {
    console.log('Now listening for request');
});